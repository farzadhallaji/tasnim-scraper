# -*- coding: utf-8 -*-
import scrapy



class MainnewsSpider(scrapy.Spider):
    name = 'mainNews'
    allowed_domains = ['www.tasnimnews.com']
    start_urls = ['http://www.tasnimnews.com/']

    def parse(self, response):

    	articles = response.css('article')
    	for a in articles: 
    		link = a.xpath('.//a/@href').extract_first()
    		art_link = response.urljoin(link)

    		yield scrapy.Request(url=art_link, callback=self.parse2)



    def parse2(self, response):

    		art_res = response
    		t1 = art_res.xpath('//*[@class="single-news"]')

    		title = t1.xpath('.//*[@class="title"]//text()').extract_first()
    		lead = t1.xpath('.//*[@class="lead"]//text()').extract_first()
    		date = art_res.xpath('//*[@class="time"]//text()').extract_first()

    		main_text = art_res.xpath('//*[@style="text-align:justify"]/text()').extract()

    		yield {
                'title':		title ,
                'lead': 		lead ,
                'date': 		date ,
                'main_text' :	main_text
            }

